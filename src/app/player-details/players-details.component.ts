import { Component, OnInit, Inject } from 'ng-metadata/core';
import { Title } from 'ng-metadata/platform-browser-dynamic';

import { PlayersService } from '../shared/players.service';
import { PlayerModel } from '../shared/player.model';

@Component({
    selector: 'my-player-details',
    moduleId: module.id,
    templateUrl: 'players-details.component.html'
})
export class PlayerDetailsComponent implements OnInit {

    player: PlayerModel;

    constructor(
        private _titleService: Title,
        private _playersService: PlayersService,
        @Inject('$stateParams') private _$stateParams: any) { }

    ngOnInit() {
        console.log('PlayerDetailsComponent initialized');

        console.log(this._$stateParams);

        let playerId = this._$stateParams.id;
        if (!playerId) {
            return;
        }

        this.player = this._playersService.getPlayer(playerId);
        this._titleService.setTitle('Player ' + this.player.name);
    }
}