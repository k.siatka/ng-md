import { Component, OnInit, Input, Output, EventEmitter } from 'ng-metadata/core';
import { Title } from 'ng-metadata/platform-browser-dynamic';

import { PlayerModel } from '../shared/player.model';

@Component({
    selector: 'my-player',
    moduleId: module.id,
    templateUrl: 'player.component.html'
})
export class PlayerComponent implements OnInit {

    @Input() player: PlayerModel;
    @Output() onSelected = new EventEmitter<PlayerModel>();

    ngOnInit() {
        console.log('PlayerComponent initialized');;
    }

    selected($event: ng.IAngularEvent) {
        $event.preventDefault();
        this.onSelected.emit(this.player);
    }
}