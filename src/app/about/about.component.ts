import { Component, OnInit, Inject } from 'ng-metadata/core';
import { NgForm } from 'ng-metadata/common';
import { Title } from 'ng-metadata/platform-browser-dynamic';

@Component({
    selector: 'my-about',
    moduleId: module.id,
    templateUrl: 'about.component.html'
})
export class AboutComponent implements OnInit {

    contactForm: NgForm;
    contact: ContactModel = new ContactModel();
    isNotificationVisible = false;

    constructor(
        private _titleService: Title, 
        @Inject('$timeout') private _$timeout: ng.ITimeoutService) { }

    submit() {
        this.contact = new ContactModel();
        this.contactForm.$setUntouched();
        this.isNotificationVisible = true;
        this._$timeout(() => this.isNotificationVisible = false, 5000);
    }

    ngOnInit() {
        console.log('AboutComponent initialized');
        this._titleService.setTitle('About');
    }

    cancelNotification() {
        this.isNotificationVisible = false;
    }
}

class ContactModel {
    email: string;
    message: string;
}