import { Component, OnInit, Inject } from 'ng-metadata/core';
import { Title } from 'ng-metadata/platform-browser-dynamic';

import { PlayersService } from '../shared/players.service';
import { PlayerModel } from '../shared/player.model';

@Component({
    selector: 'my-players',
    moduleId: module.id,
    templateUrl: 'players.component.html'
})
export class PlayersComponent implements OnInit {

    players: PlayerModel[];

    constructor(
        private _titleService: Title,
        private _playersService: PlayersService,
        @Inject('$state') private _$state: any) { }

    ngOnInit() {
        console.log('PlayersComponent initialized');
        this._titleService.setTitle('Players');
        this.players = this._playersService.getPlayers();
    }

    selected($event: PlayerModel) {
        this._$state.go('player', { id: $event.id });
    }
}