import { NgModule } from 'ng-metadata/core';
import { AsyncPipe } from 'ng-metadata/common';
import { Title } from 'ng-metadata/platform-browser-dynamic';
import * as uiRouter from 'angular-ui-router';

import { provideState } from './state.config';
import { PlayersService } from './shared/players.service';
import { AppComponent } from './app.component';
import { PlayersComponent } from './players/players.component';
import { PlayerComponent } from './player/player.component';
import { PlayerDetailsComponent } from './player-details/players-details.component';
import { AboutComponent } from './about/about.component';

@NgModule({
    imports: [uiRouter],
    providers: [
        Title,
        provideState,
        PlayersService
    ],
    declarations: [
        AsyncPipe,
        AppComponent,
        PlayersComponent,
        PlayerComponent,
        PlayerDetailsComponent,
        AboutComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }