provideState.$inject = ['$stateProvider', '$urlRouterProvider'];
export function provideState($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/players");

    $stateProvider
        .state('players', {
            url: '/players',
            template: '<my-players></my-players>'
        })
        .state('player', {
            url: '/player/:id',
            template: '<my-player></my-player>'
        })
        .state('about', {
            url: '/about',
            template: '<my-about></my-about>'
        });
}