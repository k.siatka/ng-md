import { Injectable } from 'ng-metadata/core';

import { PlayerModel } from './player.model';

@Injectable()
export class PlayersService {

    private _players: PlayerModel[];

    constructor() {

        this._players = [
            {
                id: 1,
                name: 'Jimmy Page',
                band: 'Led Zeppelin',
                photoUrl: 'http://static.thetoptens.com/img/lists/19224.jpg',
                bio: 'James Patrick Page is an English musician, songwriter, and record producer who achieved international success as the guitarist and founder of the rock band Led Zeppelin.'
            },
            {
                id: 2,
                name: 'Eddie Van Halen',
                band: 'Van Halen',
                photoUrl: 'http://static.thetoptens.com/img/lists/688.jpg',
                bio: 'Edward Lodewijk "Eddie" Van Halen is a Dutch-American musician, songwriter and producer. He is best known as the lead guitarist, occasional keyboardist and co-founder of the hard rock band Van Halen.'
            },
            {
                id: 3,
                name: 'Jimi Hendrix',
                photoUrl: 'http://static.thetoptens.com/img/lists/12178.jpg',
                bio: 'Jimi Hendrix (born November 27, 1942 - September 18, 1970) was an American rock guitarist, singer, and songwriter . Although his mainstream career spanned only four years, he is widely regarded as one of the most influential electric guitarists in the history of popular music, and one of the most celebrated'
            },
            {
                id: 4,
                name: 'Slash',
                band: 'Guns \'N Roses',
                photoUrl: 'http://static.thetoptens.com/img/lists/19590.jpg',
                bio: 'Saul Hudson, known professionally as Slash, is a British-American musician and songwriter. He is best known as the lead guitarist of the American hard rock band Guns N\' Roses, with whom he achieved worldwide success in the late 1980s and early 1990s'
            },
            {
                id: 5,
                name: 'Synster Gates',
                band: 'Avenged Sevenfold',
                photoUrl: 'http://static.thetoptens.com/img/lists/2000.jpg',
                bio: 'Brian Elwin Haner, Jr., better known by his stage name Synyster Gates or simply Syn, is an American musician, best known for being the lead guitarist and backing vocalist of the band Avenged Sevenfold.'
            },
            {
                id: 6,
                name: 'Brian May',
                band: 'Queens',
                photoUrl: 'http://static.thetoptens.com/img/lists/14341.jpg',
                bio: 'Brian Harold May is an English musician, singer, songwriter and astrophysicist, best known as the lead guitarist of the rock band Queen.'
            },
            {
                id: 7,
                name: 'Eric Clapton',
                photoUrl: 'http://static.thetoptens.com/img/lists/1456.jpg',
                bio: 'Eric Patrick Clapton is an English rock and blues guitarist, singer and songwriter. He is the only three-time inductee to the Rock and Roll Hall of Fame: once as a solo artist and separately as a member of the Yardbirds and Cream. He has also been a member of Derek and the Dominos.'
            },
            {
                id: 8,
                name: 'Kirk Hammet',
                band: 'Metallica',
                photoUrl: 'http://static.thetoptens.com/img/lists/21541.jpg',
                bio: 'Kirk Lee Hammett is the lead guitarist and songwriter for the heavy metal band Metallica and has been a member of the band since 1983. Before joining Metallica he formed and named the band Exodus.'
            },
            {
                id: 9,
                name: 'David Gilmour',
                band: 'Pink Floyd',
                photoUrl: 'http://static.thetoptens.com/img/lists/19591.jpg',
                bio: 'David Jon Gilmour is an English singer, songwriter, composer, multi-instrumentalist, and record producer. He joined the progressive rock band Pink Floyd as guitarist and co-lead vocalist in 1968.'
            },
            {
                id: 10,
                name: 'Angus Young',
                band: 'AC⚡DC',
                photoUrl: 'http://static.thetoptens.com/img/lists/31695.jpg',
                bio: 'Angus McKinnon Young is an Australian guitarist of Scottish origin, best known as the co-founder, lead guitarist, songwriter and sole constant member of the Australian hard rock band AC/DC.'
            },
            {
                id: 11,
                name: 'Tom Morello',
                band: 'Rage Against the machine',
                photoUrl: 'http://static.thetoptens.com/img/lists/8406.jpg',
                bio: 'Thomas Morello is an American musician, singer-songwriter and political activist. He is best known for his tenure with the band Rage Against the Machine and then with Audioslave.'
            },
            {
                id: 12,
                name: 'Randy Rhoads',
                band: 'Ozzy Osbourne\'s Band',
                photoUrl: 'http://static.thetoptens.com/img/lists/25766.jpg',
                bio: 'Randall William "Randy" Rhoads was an American heavy metal guitarist who played with Ozzy Osbourne and Quiet Riot.'
            }
        ];
    }

    getPlayers(): PlayerModel[] {
        return this._players;
    }

    getPlayer(id: number): PlayerModel {
        return this._players.find(p => p.id == id);
    }
}