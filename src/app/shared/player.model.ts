export class PlayerModel {
    id: number;
    name: string;
    band?: string;
    photoUrl?: string;
    bio?: string;
}