import { Component, OnInit } from 'ng-metadata/core';

@Component({
    selector: 'my-app',
    moduleId: module.id,
    templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {

    ngOnInit() {
        console.log('AppComponent initialized');
    }
}