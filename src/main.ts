import 'angular'; // import angular globally
import { platformBrowserDynamic } from 'ng-metadata/platform-browser-dynamic';

import { AppModule } from './app/app.module';


const platform = platformBrowserDynamic();
platform.bootstrapModule(AppModule);