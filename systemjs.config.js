System.config({
    baseURL: '/',
    paths: {
        'npm:*': 'node_modules/*'
    },
    map: {
        'app': './src',
        'angular': 'npm:angular/angular.min.js',
        'angular-ui-router': 'npm:angular-ui-router/release/angular-ui-router.min.js',
        'ng-metadata': 'npm:ng-metadata',
        'rxjs': 'npm:rxjs'
    },
    packages: {
        app: {
            main: './main',
            defaultExtension: 'js'
        },
        'ng-metadata': {
            defaultExtension: 'js'
        },
        'rxjs': {
            main: 'index.js',
            defaultExtension: 'js'
        }
    },
    meta: {
        'angular': {
            'format': 'global',
            'exports': 'angular'
        }
    },
});